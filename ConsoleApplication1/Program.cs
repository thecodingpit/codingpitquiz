﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
namespace ConsoleApplication1
{
    class Program
    {
        static void Main(string[] args)
        {
            List<int> resultlist = new List<int>();
            IEnumerable<int> numbers = Enumerable.Range(1, 100000).Select(x=>x);
            foreach(var nbr in numbers)
                resultlist.Add(FindResultant(nbr));

           
            Console.WriteLine("{0}", resultlist.Sum());
            Console.ReadLine();
        }

        private static int FindResultant(int nbr)
        {
            if (nbr <= 9) return nbr;

            IEnumerable<string> nbrAsStringList = nbr.ToString().Select(x => x.ToString());

            var evens = (nbrAsStringList.Where(digit => int.Parse(digit) % 2 == 0).Where(digit => int.Parse(digit) > 0)).ToArray();
            var odds = (nbrAsStringList.Where(digit => int.Parse(digit) % 2 != 0).Where(digit => int.Parse(digit) > 0)).ToArray();

            var evennbrs = Array.ConvertAll(evens, int.Parse).Sum();
            var oddnbrs = Array.ConvertAll(odds, int.Parse).Aggregate(1, (x,y)=> x*y);

            var diff = Math.Abs(evennbrs - oddnbrs);

            if(diff > 9)
                return FindResultant(diff);
            else
                return diff;
        }
    }
}
